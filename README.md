# Teste Backend - Boletos

*nomenclatura utilizada*
Pensando em desenvolver em inglês, tive que fazer algumas associações já que não existem palavras que descrevem exatamente o mesmo conceito. A lista abaixo serve para referência caso haja alguma dúvida na leitura do código:

 1. Boleto: "ticket"
 2. Boletos de arrecadação de concessionárias: "concession ticket"
 3. Boletos de títulos bancários: "bank ticket"

Não acho errado misturar as duas línguas, ainda mais com palavras sem tradução, mas quando possível, prefiro não misturar.

# Instruções

-	Clone o repositório com o comando `git clone https://bitbucket.org/matheusfonseca/boleto-ewally`
- Faça a instalação das dependências com o comando `npm install`
- Inicie a aplicação com o comando `npm start`
- Faça um request **POST** para a URL *localhost:3000/boleto* com um body com a seguinte estrutura `{ "code": "23790.50400 42000.219149 34008.109208 5 81730000019900" }`, podendo o código ser tanto de um título bancário ou de uma arrecadação de concessionárias.

Exemplos de códigos para testes: 

- Título bancário: 23790.50400 42000.219149 34008.109208 5 81730000019900 (47 NUMEROS)
- Arrecadação de concessionárias (linha digitável): 85890000460-9 52460179160-5 60759305086-5 83148300001-0 (48 NUMEROS)