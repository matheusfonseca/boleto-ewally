const identifier = require('../services/identification/identifier')

const bankValidator = require('../services/bank/validation')
const concessionValidator = require('../services/concession/validation')
const bankExtractInfo = require('../services/bank/extractInfo')
const concessionExtractInfo = require('../services/concession/extractInfo')

const codeParser = code => {
  return code.replace(/[\-. ]/g, '')
}


const boletoController = async (req, res) => {
  const code = req.body.code
  console.log('código', code)
  const parsedCode = codeParser(code)
  console.log('código parseado', parsedCode)
  const boletoType = identifier(parsedCode)

  let validCode, result
  console.log(boletoType)
  if (boletoType === 'TITULO_BANCARIO') { 
    console.log('é titulo')
    validCode = bankValidator(parsedCode)

    result = validCode ? bankExtractInfo(parsedCode) : false
  }
  if (boletoType === 'ARRECADACAO_CONCESSIONARIA') {
    console.log('é concessão')
    validCode = concessionValidator(parsedCode)

    result = validCode ? concessionExtractInfo(parsedCode) : false
  }
  if (boletoType === false) {
    result = false
  }
  
  !result ? res.status(400).send('Invalid code') : res.status(200).send(result)
}

module.exports = boletoController
