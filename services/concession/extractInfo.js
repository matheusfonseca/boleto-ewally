const moment = require('moment')

const infos = (code) => {
  const barcode = (code) => {
    let convertedBarcode = ''
    for (let numberBlock = 0; numberBlock < 4; numberBlock++) {
      const start = (11 * (numberBlock)) + numberBlock
      const end = (11 * (numberBlock + 1)) + numberBlock
      convertedBarcode += code.substring(start, end)
    }
    return convertedBarcode
  }

  const value = (code) => {
    return (parseInt(code.substring(4, 15), 10) / 100).toFixed(2).replace('.', ',')
  }

  const expire = (code) => {
    // vai parar de funcionar em 2025
    const date = barcode(code).substring(19, 27)
    const result = moment(date, 'YYYYMMDD')
    
    return result
  }

  return {
    barcode: barcode(code),
    value: value(code),
    expireDate: expire(code)
  }
}


module.exports = infos
