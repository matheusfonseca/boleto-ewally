const { modulus10, modulus11Concession } = require('../identification/verificationDigit')

const barcode = (code) => {
  console.log(code)
  if (Number(code[0]) !== 8) return false
  const currencyCode = Number(code[2])
  const verificationDigit = Number(code[3])
  const codeForVerification = code.substring(0, 3) + code.substring(4)
  let verification
  (currencyCode === 6 || currencyCode === 7) ? verification = modulus10(codeForVerification) :
    (currencyCode === 8 || currencyCode === 9) ?  verification = modulus11Concession(codeForVerification) : false

  //console.log(verification(codeForVerification), verificationDigit)
  console.log('passou')
  console.log(verification)
  return verification === verificationDigit
}

const digitableLine = (code) => {
  let convertedToBarcode = ''
  for (let numberBlock = 0; numberBlock < 4; numberBlock++) {
    const start = (11 * (numberBlock)) + numberBlock;
    const end = (11 * (numberBlock + 1)) + numberBlock;
    convertedToBarcode += code.substring(start, end);
    console.log(convertedToBarcode)
  }
  
  return barcode(convertedToBarcode)
}

module.exports = digitableLine