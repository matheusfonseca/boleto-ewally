const infos = (code) => {
  const barcode = (code) => {
    let convertedBarcode
    for (let numberBlock = 0; numberBlock < 4; numberBlock++) {
      const start = (11 * (numberBlock)) + numberBlock
      const end = (11 * (numberBlock + 1)) + numberBlock
      convertedBarcode += code.substring(start, end)
    }
    return convertedBarcode
  }

  const value = (code) => {
    return parseInt(code.substring(37, 10))
  }

  const expire = (code) => {
    // vai parar de funcionar em 2025
    const days = code.substring(6, 9)
    let result = new Date('10/07/1997')
    result.setDate(result.getDate() + days)
    return result
  }

  return {
    barcode: barcode(code),
    value: value(code),
    expireDate: expire(code)
  }
}

module.exports = infos
