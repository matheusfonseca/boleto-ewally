const { modulus10, modulus11Bank } = require('../identification/verificationDigit')

const barcode = (code) => {
  const verificationDigit = code[4]
  const codeForVerification = code.substring(0, 4) + code.substring(5)
  return modulus11Bank(codeForVerification) === Number(verificationDigit)
}

const digitableLine = (code) => {
  const chunks = [
    {
      num: code.substring(0, 9),
      DV: code.substring(9, 10),
    },
    {
      num: code.substring(10, 20),
      DV: code.substring(20, 21),
    },
    {
      num: code.substring(21, 31),
      DV: code.substring(31, 32),
    },
  ]
  const validateChunks = chunks.every(e => modulus10(e.num) === Number(e.DV))

  let convertedToBarcode = '';
  convertedToBarcode += code.substring(0, 3); // Identificação do banco
  convertedToBarcode += code.substring(3, 4); // Código da moeda
  convertedToBarcode += code.substring(32, 33); // DV
  convertedToBarcode += code.substring(33, 37); // Fator Vencimento
  convertedToBarcode += code.substring(37, 47); // Valor nominal
  convertedToBarcode += code.substring(4, 9); // Campo Livre Bloco 1
  convertedToBarcode += code.substring(10, 20); // Campo Livre Bloco 2
  convertedToBarcode += code.substring(21, 31); // Campo Livre Bloco 3

  const validVerificationDigit = barcode(convertedToBarcode)

  return validateChunks && validVerificationDigit
}

module.exports = digitableLine