const typeBoleto = code => {
  
  switch (code.length) {
    case 47:
      return 'TITULO_BANCARIO'
    case 48:
      return 'ARRECADACAO_CONCESSIONARIA'
    default:
      return null
  }
}

module.exports = typeBoleto