const modulus10 = (chunk) => {
  console.log('chunk',chunk)
  const codigo = chunk.split('').reverse()
  const somatorio = codigo.reduce((acc, current, index) => {
    let soma = Number(current) * (((index + 1) % 2) + 1)
    soma = (soma > 9 ? Math.trunc(soma / 10) + (soma % 10) : soma)
    return acc + soma
  }, 0)
  return (Math.ceil(somatorio / 10) * 10) - somatorio
}

const modulus11Concession = (chunk) => {
  console.log(chunk)
  const codigo = chunk.split('').reverse()
  let multiplicador = 2
  const somatorio = codigo.reduce((acc, current) => {
    const soma = Number(current) * multiplicador
    multiplicador = multiplicador === 9 ? 2 : multiplicador + 1
    return acc + soma
  }, 0)
  const restoDivisao = somatorio % 11
  console.log(chunk)
  if (restoDivisao === 0 || restoDivisao === 1) {
    return 0
  }
  if (restoDivisao === 10) {
    return 1
  }
  const DV = 11 - restoDivisao
  return DV
}

const modulus11Bank = (chunk) => {
  const codigo = chunk.split('').reverse()
  let multiplicador = 2
  const somatorio = codigo.reduce((acc, current) => {
    const soma = Number(current) * multiplicador
    multiplicador = multiplicador === 9 ? 2 : multiplicador + 1
    return acc + soma
  }, 0)
  const restoDivisao = somatorio % 11
  const DV = 11 - restoDivisao
  if (DV === 0 || DV === 10 || DV === 11) return 1
  return DV
}

module.exports = {
  modulus10,
  modulus11Bank,
  modulus11Concession
}