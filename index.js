const express = require('express')

const boletoController = require('./controllers/boletoController')

const app = express()
const port = 3000

app.use(express.json())

app.post('/boleto', boletoController)

app.listen(port, () => console.log(`app online`))

